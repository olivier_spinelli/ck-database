using CK.Core;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.Testing.StObjSetup
{
    /// <summary>
    /// Defines the event argument when starting a StObjSetup.
    /// </summary>
    public class StObjSetupRunningEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new event with an existing configuration.
        /// </summary>
        /// <param name="conf">The configuration.</param>
        /// <param name="forceSetup">Initial <see cref="ForceSetup"/> configuration.</param>
        public StObjSetupRunningEventArgs( StObjEngineConfiguration conf, bool forceSetup )
        {
            StObjEngineConfiguration = conf;
            ForceSetup = forceSetup;
        }

        /// <summary>
        /// Gets a mutable <see cref="StObjEngineConfiguration"/>.
        /// This object is configured with the different values of <see cref="IStObjSetupTestHelperCore"/>.
        /// Its <see cref="StObjEngineConfiguration.Aspects"/> is empty: some aspect configurations must be
        /// added.
        /// </summary>
        public StObjEngineConfiguration StObjEngineConfiguration { get; }

        /// <summary>
        /// Gets or sets the <see cref="CKSetup.ICKSetupDriver.Run"/> forceSetup parameter.
        /// </summary>
        public bool ForceSetup { get; set; }
    }
}
