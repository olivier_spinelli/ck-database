using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CK.Core
{
    interface IStObjServiceFinalManualMapping : IStObjServiceClassFactory
    {
        int Number { get; }
    }

}
