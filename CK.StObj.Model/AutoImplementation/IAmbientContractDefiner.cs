﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CK.Core
{
    /// <summary>
    /// Auto implemented type marker that should be supported by any automatically generated type.
    /// This allow easy detection of such types.
    /// </summary>
    public interface IAutoImplementedType
    {
    }


}
